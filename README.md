# Idées

* Si répertoire git :
  * donne la branche courante
  * le dernier message de commit
  * le status (e.g. fichiers non stagés, non commité, devant ou derrière le HEAD, etc.. )
* Dernier fichiers modifiés avec info de quand
* % des fichiers qui nous appartiennent
* l'espace occupé sur le disque