echocolor() {
	echo -e "\e[${1}m${2}\e[39m"
}

color_red=31

echocolor 33 "Storage size: $(du -sh . | tail -1 | cut -f1)"
echocolor 34 "Current branch: $(git rev-parse --abbrev-ref HEAD)"
echocolor 35 "Commit message: $(git log -1 --pretty=%B | head -1)"
echocolor $color_red "Git status:\n$(git status -s)"
echocolor 37 "Git modified number:$(git status -s |grep " M " |wc -l)"
echo -e "Git log: $(git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all)"
echo $(git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --color)
echo -e "last modified file: $(ls --color -tr | tail -1)"